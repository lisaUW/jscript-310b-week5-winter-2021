// Given the <body> element as variable body,
// access the <main> node and log to the console.
const body = document.querySelector('body');
let main = body.childNodes[0];
console.log(main);

// Given the <ul> element as variable ul,
// access <body>  and log to the console.
const ul = document.querySelector('ul');
let body = body.parentNode.parentNode;
console.log(body);

// Given the <p> element as var p,
// access the 3rd <li>  and log to the console.
const p = document.querySelector('p');
let li = p.previousSibling.childNodes[2];
console.log(li);
