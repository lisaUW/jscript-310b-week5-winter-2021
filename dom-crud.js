// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let a = document.createElement("a");   
a.innerHTML = "Buy Now!";
a.id = "cta";

let ps = document.getElementsByTagName("p");

let lastP = ps[ps.length -1];

ps.lastP.id = "cta";

lastP.appendChild(a);
// Access (read) the data-color attribute of the <img>,
// log to the console
let img = document.getElementsByTagName("img")[0];

let attribute = img.getAttribute("data-color");
console.log(attribute);
// Update the third <li> item ("Turbocharged"), 

document.getElementsByTagName('li')[2].innerHTML = "Supercharged";

// set the class name to "highlight"
document.getElementsByTagName('li')[2].className = "highlight";

// Remove (delete) the last paragraph
var p = document.getElementsByTagName('p')[0];
p.remove();
// (starts with "Available for purchase now…")
