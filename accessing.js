// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
let header = document.getElementById("weather-head");

header.innerText = "February 10 Weather Forecast, Seattle";

// Change the styling of every element with class "sun" to set the color to "orange"
let elements = document.getElementsByClassName("sun");

let i;
for (i = 0; i < elements.length; i++) {
    elements[i].style.color = "orange";
};

// Change the class of the second <li> from to "sun" to "cloudy"


document.getElementsByTagName("li")[1].className = "cloudy";