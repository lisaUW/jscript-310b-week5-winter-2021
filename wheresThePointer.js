// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click


let body = document.getElementsByTagName('body')[0];

body.addEventListener('click', event => {
    console.log(event.target.tagName);
  if (event.target.tagName === 'TD') {
    event.target.innerHTML = `${event.pageX}, ${event.pageY}`
  }
  //handle click
});